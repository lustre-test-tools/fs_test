%global debug_package %{nil}
Summary: fs_test benchmark
name: fs_test
Version: 1.112
Release: 1%{?dist}
License: GPL
Group: Utilities/Benchmarking
URL: https://github.com/fs-test/fs_test
Source: %{name}-%{version}.tar.gz
BuildRoot: /tmp/%{name}-buildroot
BuildRequires:  mpich-devel
Requires:       mpich

%description
fs_test benchmark

%prep
%setup -q

%build
%{?_mpich_load}
make
%{?_mpich_unload}

%install

%{?_mpich_load}

%if 0%{?rhel} != 8
MPI_BIN=%{_bindir}
%endif

rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}${MPI_BIN}
install -d ${RPM_BUILD_ROOT}${MPI_BIN}
install -s -m 755 %{name}.x ${RPM_BUILD_ROOT}${MPI_BIN}
%{?_mpich_unload}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%if 0%{?rhel} == 8
%{_libdir}/mpich/bin/%{name}.x
%else
%{_bindir}/%{name}.x
%endif

%changelog
